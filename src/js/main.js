/* NAV LI AUTO SCROLLING */
var $root = $('html, body');
$('a[href^="#"]').click(function () {
    $root.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 400);

    return false;
});
/* NAV LI AUTO SCROLLING */


/* MOBILE MENU */
$('#ikona').click( function() {
    var x = $('#ikonaThing').attr('class');
    var saveClasS;  //returning the exact class I found once I've clicked ('topnav' or 'topnav scrolled')
    console.log(x);
    
    if (x === "topnav" || x === 'topnav scrolled') {
        saveClasS = x;
        $("#ikonaThing").attr('class', 'openedNav');
        $(".openedNav li").hide();
        $(".openedNav li a").hide();
        $('.wrappEverything').not('openedNav').fadeTo( "slow" , 0.5, function() {
            // Animation complete.
          });
        $('.openedNav li').slideDown(300);
        $('.openedNav li a').delay(300).show(300);
        $("#scrollUpIconId").attr('class', 'far fa-arrow-alt-circle-up');
    } else {
        $('.wrappEverything').not('openedNav').fadeTo( "slow" , 1, function() {
            // Animation complete.
          });
        if(saveClasS === "topnav") {
            $("#ikonaThing").attr('class', 'topnav');
            $("#scrollUpIconId").attr('class', 'fa fa-bars');
        } else {
            $("#ikonaThing").attr('class', 'topnav scrolled');
            $("#scrollUpIconId").attr('class', 'fa fa-bars');
        }

    }
});


/**
 * Checking on changing width while scrolled down menu is open
 */
$(window).resize(function () {
    setServiceSection();
});

function setServiceSection() {
    if ($(window).width() > 800) {
        $("#ikonaThing").attr('class', 'topnav scrolled');
            $("#scrollUpIconId").attr('class', 'fa fa-bars');
         
    } else {
      

    }
};




/* MOBILE MENU */



/* PRELOAD SCREEN GIF */
setTimeout( function() {
    $('.preloadingWrapper').fadeOut();
}, 3000);
$( document ).ready(function() {
    visitWebsiteGif();
}); 

function visitWebsiteGif() {
    lottie.loadAnimation({
        container: document.getElementById('lottieVisitWebsite'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: 'src/json/animated_laptop.json' // the path to the animation json
    });
    lottie.play
}; 
/* PRELOAD SCREEN GIF */




/* CONTACT EMAIL SENDER */
var EMAIL_URL = 'https://liberasoft.eu:8536/sendEmail';
$('#emailRequest').click( function(e) {
    e.preventDefault();
    

    var dataArray = {
        name: $('#name').val(),
        email: $('#email').val(),
        website: $('#website').val(),
        message: $('#message').val() 
    }
   
    $.ajax({
        type: "POST",
        url: EMAIL_URL,
        dataType: 'text',
        async: true,
        data: dataArray,

        success: function (data) {
            $('#emailReponse').text('Email sent! Thank you for contacting us.')
            $('#name').val(''),
            $('#email').val(''),
            $('#website').val(''),
            $('#message').val('')
        },
        error: function (jqXHR, textStatus, err) {
            $('#emailReponse').text('Sorry, could not send mail! Please contact us on +385 95 530 3900')
        }

    });
});
/* CONTACT EMAIL SENDER */


/* GET REQUEST VISITORS COUNTER */
var COUNT_VISITORS_URL = 'https://liberasoft.eu:4784';
$(document).ready( function() {
   
    $.ajax({
        type: "POST",
        url: COUNT_VISITORS_URL,
        dataType: 'text',
        async: true,
        data: "{count: 'visitor'}",

        success: function (data) {
            console.log('Welcome developer :)');
            
        },
        error: function (jqXHR, textStatus, err) {
        }

    });
});
/* GET REQUEST VISITORS COUNTER */
