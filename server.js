const express = require('express');
const app = express();
const path = require('path');
const port = 5677;

const public = path.join(__dirname, 'src');

app.get('/', function (req, res) {
  res.sendFile(path.join(public, 'index.html'));
});

app.use('/', express.static(public));

app.listen(port);
console.log(`Served website on port ${port}`);
